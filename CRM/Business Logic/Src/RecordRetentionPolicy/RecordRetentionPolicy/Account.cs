﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class Account
    {
        //KJS: I spoke with Greg today and we should exclude deleting any accounts with the 
        //Account Type of Contractor, Utility, Other, or Sub Contractor
        public const string EntityName = "account";
        public const string AccountName = "name";
        public const string AccountPrimaryKey = "accountid";
        public const string AccountNumber = "accountnumber";
        public const string ParentAccountID = "parentaccountid"; //Choose the parent account associated with this account to show parent and child businesses in reporting and analytics.
        public const string CreatedOn = "createdon";
        public const string AccountTypeContractor = "seera_contractor";
        public const string AccountTypeUtility = "seera_utility";
        public const string AccountTypeSubContractor = "seera_subcontractor";
        public const string AccountTypeCustomer = "seera_customer";
        public const string AccountTypeProvider = "seera_provider";
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old
    }
}
