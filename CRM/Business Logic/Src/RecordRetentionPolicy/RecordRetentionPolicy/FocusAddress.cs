﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class FocusAddress
    {
        public const string EntityName = "seera_seeraaddresses";
        public const string FocusAddressPrimaryKey = "seera_seeraaddressesid";  //Unique identifier for entity instances 
        public const string FocusAddressID = "seera_focusaddressid";//Auto-generated identification number assigned to the record on creation.
        public const string ParentAccountID = "seera_accountid"; //Unique identifier for Account associated with SEERA Addresses.
    }
}
