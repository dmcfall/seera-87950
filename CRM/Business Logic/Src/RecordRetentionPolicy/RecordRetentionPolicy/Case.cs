﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class Case
    {
        public const string EntityName = "incident";
        public const string CasePrimaryKey = "incidentid";  //Unique identifier of the case. 
        public const string ParentAccountID = "seera_accountthiscaseisregarding";  //Unique identifier for Account associated with Case.
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old
        public const string CreatedOn = "createdon";//date that the case was created  
        public const string CaseNumber = "ticketnumber";//Shows the case number for customer reference and searching capabilities. This cannot be modified.  
        
    }
}
