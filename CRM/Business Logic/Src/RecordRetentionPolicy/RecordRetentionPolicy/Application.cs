﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class Application
    {
        public const string EntityName = "seera_application";
        public const string ApplicationPrimaryKey = "seera_applicationid";    //Unique identifier for entity instances
        public const string FocusApplicationName = "seera_focusapplicationname";  //Custom name field populated by the User.
        public const string ApplicationID = "seera_id";   //Auto-generated identification number assigned to the record on creation.       
        public const string ApplicationName = "seera_name";   //Auto-generated combination of the Application ID and the Focus Application Name fields.
        public const string Customer = "seera_customerid";    //Unique identifier for Account associated with Application.
        public const string CreatedOn = "createdon";
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old 
    }
}
