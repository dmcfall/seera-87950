﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class Opportunity
    {
        public const string EntityName = "opportunity";
        public const string OpportunityPrimaryKey = "opportunityid";  //Unique identifier of the opportunity. 
        public const string ParentAccountID = "parentaccountid";  //Unique identifier for Account associated with opportunity.
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old
        public const string CreatedOn = "createdon";//date that the case was created    
        public const string OpportunityName = "name";//date that the case was created  
    }
}
