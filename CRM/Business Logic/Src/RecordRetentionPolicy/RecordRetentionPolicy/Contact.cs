﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class Contact
    {
        public const string EntityName = "contact";
        public const string ContactPrimaryKey = "contactid";  //Unique identifier of the contact. 
        public const string ParentAccountID = "parentcustomerid"; //Select the parent account or parent contact for the contact to provide a quick link to additional details, such as financial information, activities, and opportunities.
        public const string FullName = "fullname"; //Unique identifier for address 1.
        public const string ContactID = "seera_contactid"; //Auto-generated identification number assigned to the record on creation. This number is combined with the Focus Address ID and sent to GP inorder to have the check addressed to a specific contact.
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old
        public const string CreatedOn = "createdon";
    }
}
