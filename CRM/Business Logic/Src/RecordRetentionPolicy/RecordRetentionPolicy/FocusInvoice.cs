﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class FocusInvoice
    {
        public const string EntityName = "seera_invoice";
        public const string FocusInvoicePrimaryKey = "seera_invoiceid";  //Unique identifier for entity instances
        public const string FocusInvoiceID = "seera_invoicenumber"; //Auto-generated identification number assigned to the record on creation.
        public const string FocusInvoiceAccountID = "seera_accountid"; //Unique identifier for Account associated with Invoice.
        public const string CreatedOn = "createdon";
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old
    }
}
