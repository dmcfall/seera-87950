﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class DocumentLocation
    {
        public const string EntityName = "sharepointdocumentlocation";
        public const string SharePointDocumentLocationPrimaryKey = "sharepointdocumentlocationid";//Unique identifier of the SharePoint document location record.
        public const string Regarding = "regardingobjectid";  //Unique identifier of the object with which the SharePoint document location record is associated. 
        public const string ParentSiteOrLocation = "parentsiteorlocation"; //Unique identifier of the parent site or location.
        public const string RelativeURL = "relativeurl";//Relative URL of the SharePoint document location.
        public const string AbsoluteURL = "absoluteurl"; //Absolute URL of the SharePoint document location.
        public const string Name = "name"; //Name of the SharePoint document location record.        
    }
}
