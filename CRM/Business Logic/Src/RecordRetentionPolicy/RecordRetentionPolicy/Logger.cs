﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;

namespace SKICRMIntegration
{
    public class Logger
    {
        private string _logFileName;
        private string _logFileLocation;
        private FileInfo _file;

        public Logger()
        {

            _logFileName = ConfigurationManager.AppSettings.Get("LogFileName");
            _logFileLocation = ConfigurationManager.AppSettings.Get("LogFileLocation");

            //create the log file if it doesn't exist yet
            try
            {
                _file = new FileInfo(_logFileLocation + _logFileName);
                if (!_file.Exists)
                {
                    _file.Create();
                }
            }
            catch (System.IO.IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }

        public void WriteToLog(string inText)
        {

            try
            {
                using (StreamWriter writer = _file.AppendText())
                {
                    writer.WriteLine(System.DateTime.Now.ToString() + " : " + inText);
                }
            }
            catch (System.IO.IOException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        
    }
}
