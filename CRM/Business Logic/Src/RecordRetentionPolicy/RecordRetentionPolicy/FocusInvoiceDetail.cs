﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class FocusInvoiceDetail
    {
        public const string EntityName = "seera_invoicedetails";
        public const string FocusInvoiceDetailPrimaryKey = "seera_invoicedetailsid";  //Unique identifier for entity instances  
        public const string ParentFocusInvoiceID = "seera_invoicenumber"; //Unique identifier for Invoice associated with Invoice Details.
        public const string ExpenditureDate = "seera_expendituredate"; //date/month in which we count this invoice detail payment as an expenditure.  typically the date paid in GP minus one month.
        public const string CreatedOn = "createdon";
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old
        public const string InvoiceDetailName = "seera_name"; //Invoice Number gets copied into this name field.
        public const string InvoiceDetailID = "seera_invoicedetailid"; //Invoice Number gets copied into this name field.
    }
}
