﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class ApplicationMeasure
    {
        public const string EntityName = "seera_applicationmeasures";
        public const string ApplicationMeasurePrimaryKey = "seera_applicationmeasuresid";    //Unique identifier for entity instances
        public const string ApplicationMeasureApplicationID = "seera_app_measuresid";   //Unique identifier for Application associated with Application Measures.
        public const string ApplicationMeasureCustomerID = "seera_customer"; //Unique identifier for Account associated with Application Measure.
        public const string CreatedOn = "createdon";
        public const string DateOfStatusChangeToPaid = "seera_paymentpaiddate"; //Date the application measure was Paid (date that the measure status was changed to Paid).
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old
        public const string ApplicationMeasureID = "seera_applicationmeasuredocumentid"; 
    }
}
