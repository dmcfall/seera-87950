﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public static class Payment
    {
        public const string EntityName = "seera_paymentinformation";
        public const string PaymentPrimaryKey = "seera_paymentinformationid";  //Unique identifier for entity instances 
        public const string ParentApplicationMeasureID = "seera_applicationmeasureid"; //Unique identifier for Application Measures associated with Payment.
        public const string ParentInvoiceDetailID = "seera_invoicedetails"; //Unique identifier for Invoice Details associated with Payment.
        public const string ParentAccountID = "seera_marketproviderid"; //Unique identifier for Account associated with Payment Information.
        public const string PaymentID = "seera_paymentid"; //Auto-generated identification number assigned to the record on creation.
        public const string DatePaid = "seera_datepaid"; //Date of which the check number gets generated from GP and assiged to this payment record.
        public const string AmountPaid = "seera_amountpaid"; //Specific amount of dollars for payment (different from check amount as multiple payments can be consolidated by GP into one check).
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old
    }
}
