﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk;
using Microsoft.Xrm.Client.Services;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint;
using log4net;
using log4net.Config;
using System.Net;


namespace RecordRetentionPolicy
{
    public static class SharePoint
    {
        static readonly ILog Log = log4net.LogManager.GetLogger(typeof(RecordRetentionPolicy));
        private static string _baseSiteUrl;
        private static string _userName;
        private static string _password;
        

        public static bool DeleteDocumentLocationFromSharePoint(string relativeURL)
        {
            _baseSiteUrl = ConfigurationManager.AppSettings["BaseSiteUrl"];
            _userName = ConfigurationManager.AppSettings["UserName"];
            _password = ConfigurationManager.AppSettings["Password"];
            ClientContext clientContext = null;

            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {
                try
                {
                    using (clientContext = new ClientContext(_baseSiteUrl))
                    {
                        NetworkCredential credentials = new NetworkCredential(_userName, _password);
                        clientContext.Credentials = credentials;

                        List oList = clientContext.Web.Lists.GetByTitle("Account");

                        CamlQuery camlQuery = new CamlQuery();
                        camlQuery.ViewXml = "<View/>";

                        ListItemCollection collListItem = oList.GetItems(camlQuery);

                        clientContext.Load(collListItem,
                             items => items.Include(
                                item => item.Id,
                                item => item.DisplayName,
                                item => item.HasUniqueRoleAssignments));

                        clientContext.ExecuteQuery();

                        foreach (ListItem oListItem in collListItem)
                        {
                            if (oListItem.DisplayName == relativeURL)
                            {
                                try
                                {
                                    oListItem.DeleteObject();
                                    clientContext.ExecuteQuery();
                                    Log.Error("Successfully deleted SharePoint Document Location " + relativeURL);
                                    Console.WriteLine("Successfully deleted SharePoint Document Location " + relativeURL);
                                    break;
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Failed to delete SharePoint document location " + relativeURL + "ERROR:" + ex.Message);
                                    Log.Error("Failed to delete SharePoint document location " + relativeURL + "ERROR:" + ex.Message + ": " + ex.InnerException + ": " + ex.StackTrace);
                                }

                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    Log.Info("ERROR in SharePoint.DeleteDocumentFromSharePoint " + ex.Message);
                    Console.WriteLine("ERROR in SharePoint.DeleteDocumentFromSharePoint " + ex.Message);
                }
                finally
                {
                    if (clientContext != null)
                    {
                        clientContext.Dispose();
                    }
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;
        }
    }
}
