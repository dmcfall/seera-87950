﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk;
using Microsoft.Xrm.Client.Services;
using log4net;
using log4net.Config;

namespace RecordRetentionPolicy
{
    public class RecordRetentionPolicy
    {
        private OrganizationService _service;
        static readonly ILog Log = log4net.LogManager.GetLogger(typeof(RecordRetentionPolicy));

        /* Entities that could have documents in SharePoint
         *   Account
         *   Application
         *   Case
         *   Contact
         *   Lead
         *   Opportunity
         */

        static void Main(string[] args)
        {

            try
            {
                XmlConfigurator.Configure();
                String connectionString = GetServiceConfiguration();
                var RecordRetentionPolicy = new RecordRetentionPolicy();

                RecordRetentionPolicy.Controller(connectionString);
            }
            catch (Exception exception)
            {
                Log.Error(exception.Message, exception);
            }
        }


        public void Controller(string connectionString)
        {
            EntityCollection Payments = new EntityCollection();
            EntityCollection ApplicationMeasures = new EntityCollection();
            EntityCollection FocusInvoices = new EntityCollection();
            EntityCollection Applications = new EntityCollection();
            EntityCollection Cases = new EntityCollection();
            EntityCollection Opportunities = new EntityCollection();
            EntityCollection Leads = new EntityCollection();
            EntityCollection Contacts = new EntityCollection();
            EntityCollection Accounts = new EntityCollection();
            EntityCollection DocumentLocations = new EntityCollection();

            int keeperApplicationCheck = 0;
            int keeperApplicationMeasureCheck = 0;
            int keeperFocusInvoiceCheck = 0;
            int keeperCaseCheck = 0;
            int keeperOpportunityCheck = 0;
            int keeperLeadCheck = 0;
                        
            bool retBool = false;
            int retValue = 0;

            //DISABLE these plugins before executing
            //CRM.SEERA.Plugins.ApplicationMeasure.Controller.UpdateApplicationOnDelete: Delete of  any Entity  (post-operation)
            //CRM.Wipfli.BusinessLogic.seera_application_restrict_delete: Delete of  any Entity   (pre-validation)
            //CRM.Wipfli.BusinessLogic.seera_invoicedetails_delete: Delete of  any Entity    (pre-operation)


            CrmConnection connection = CrmConnection.Parse(connectionString);
            using (_service = new OrganizationService(connection))
            {

                string delete = ConfigurationManager.AppSettings["Delete"];

                Log.Info("RecordRetentionPolicy Started...");
                Log.Info("");
                Log.Info("RecordRetentionPolicy.exe executing");
                Log.Info("");
                Log.Info("Delete configuration setting currently set to " + delete);
             
                Console.WriteLine("RecordRetentionPolicy.exe executing");

                /*
                * You are deleting from the CRM Online DB.  The replicated (SEERA Desktop) db should have a copied 
                * version of the record you deleted (we need to make sure that the replicated record 
                * gets updated with the 5 year delete flag value).
                 * 
                 * Account -> Application = Referential
                 * Application -> Application Measure = Parental (cascade delete)
                 * Application Measure -> Payment = Referential
                 * 
                 * Account -> Focus Invoice = Referential
                 * Focue Invoice -> Invoice Details = Parental (cascade delete)
                 * Invoice Details -> Payment = Referential
                 * 
                 * 
                 * When Application Measures are to be deleted then all Payment records for that Application Measure get deleted.
                 * When a Focus Invoice is to be deleted then all Payment records for that Focus Invoice get deleted.
                 * 
                 * Only delete accounts with account types of "Customer","Provider", "Contractor" and "Sub Contractor" (added contractor and sub-contractor on 2/26/2015 per Kevin S.)
                 * NEVER delete accounts with account type UTILITY 
                */

                //Get Focus Invoices > five years old based on child FocusInvoiceDetail.ExpenditureDate value
                FocusInvoices = GetFocusInvoicesGreaterThanFiveYearsOld(_service);
                foreach (Entity fi in FocusInvoices.Entities)
                {
                    //debug
                    Console.WriteLine(" ");
                    Console.WriteLine("Focus Invoice ID= " + fi.Attributes[FocusInvoice.FocusInvoiceID] );
                    Log.Info(" ");
                    Log.Info("Focus Invoice ID= " + fi.Attributes[FocusInvoice.FocusInvoiceID]);

                    //Get the invoice details for the current Focus Invoice
                    EntityCollection invoiceDetails = GetInvoiceDetailsByFocusInvoiceID((Guid)fi.Attributes[FocusInvoice.FocusInvoicePrimaryKey], _service);

                    //Loop though the invoice detail collection
                    foreach (Entity id in invoiceDetails.Entities)
                    {
                        //debug
                        Console.WriteLine("Invoice Detail= " + id.Attributes[FocusInvoiceDetail.InvoiceDetailID]);
                        Log.Info("Invoice Detail= " + id.Attributes[FocusInvoiceDetail.InvoiceDetailID]);

                        //Get the collection of payments that reference the current InvoiceDetail
                        EntityCollection payments = GetPaymentsByInvoiceDetailID((Guid)id.Attributes[FocusInvoiceDetail.FocusInvoiceDetailPrimaryKey], _service);

                        //Loop though the payment collection
                        foreach (Entity p in payments.Entities)
                        {
                            //debug
                            Console.WriteLine("Payment = " + p.Attributes[Payment.PaymentID]);
                            Log.Info("Payment = " + p.Attributes[Payment.PaymentID]);

                            //set the payment five year delete flag to true on all payments that reference the current Focus Invoice
                            retBool = UpdatePaymentFiveYearDeleteFlag(_service, p);
                            if (retBool)
                            {
                                //now delete the payment record
                                retBool = DeletePayment(_service, p);
                                if (!retBool)
                                {
                                    Console.WriteLine("Failed to delete payment");
                                    Log.Info("Failed to delete payment");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Failed to update payment five year delete flag");
                                Log.Info("Failed to update payment five year delete flag");
                            }

                        }//end of Payment foreach loop

                        payments.Entities.Clear();

                        //Then set the invoice details five year delete flag to true
                        retBool = UpdateFocusInvoiceDetailFiveYearDeleteFlag(_service, id);
                        if (retBool)
                        {
                            //now delete the invoice detail record
                            retBool = DeleteInvoiceDetail(_service, id);
                            if (!retBool)
                            {
                                Console.WriteLine("Failed to delete invoice detail");
                                Log.Info("Failed to delete invoice detail");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Failed to update focus invoice detail five year delete flag");
                            Log.Info("Failed to update focus invoice detail five year delete flag");
                        }

                    }//end of Invoice Detail foreach loop

                    //Then set the Focus Invoice five year delete flag to true
                    retBool = UpdateFocusInvoiceFiveYearDeleteFlag(_service, fi);
                    if (retBool)
                    {
                        //now delete the focus invoice record
                        retBool = DeleteFocusInvoice(_service, fi);
                        if (!retBool)
                        {
                            Console.WriteLine("Failed to delete focus invoice");
                            Log.Info("Failed to delete focus invoice");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Failed to update focus invoice five year delete flag");
                        Log.Info("Failed to update focus invoice five year delete flag");
                    }

                }//end of Focus Invoice foreach loop


                //Get Applications > five years old based on child ApplicationMeasure.DateOfStatusChangeToPaid value
                Applications = GetApplicationsGreaterThanFiveYearsOld(_service);
                foreach (Entity ap in Applications.Entities)
                {
                    //debug
                    Console.WriteLine(" ");
                    Console.WriteLine("Application = " + ap.Attributes[Application.ApplicationName]);
                    Log.Info(" ");
                    Log.Info("Application = " + ap.Attributes[Application.ApplicationName]);

                    //Get the Application Measures for the current Application
                    EntityCollection applicationMeasures = GetApplicationMeasuresByApplicationID((Guid)ap.Attributes[Application.ApplicationPrimaryKey], _service);
                    foreach (Entity am in applicationMeasures.Entities)
                    {
                        //debug
                        Console.WriteLine("Application Measure = " + am.Attributes[ApplicationMeasure.ApplicationMeasureID]);
                        Log.Info("Application Measure = " + am.Attributes[ApplicationMeasure.ApplicationMeasureID]);

                        //Get the collection of payments that reference the current application Measure
                        EntityCollection payments = GetPaymentsByApplicationMeasureID((Guid)am.Attributes[ApplicationMeasure.ApplicationMeasurePrimaryKey], _service);

                        //Loop though the payment collection
                        foreach (Entity p in payments.Entities)
                        {
                            //debug
                            Console.WriteLine("Payment = " + p.Attributes[Payment.PaymentID]);
                            Log.Info("Payment = " + p.Attributes[Payment.PaymentID]);

                            //set the payment five year delete flag to true on all payments that reference the current Focus Invoice
                            retBool = UpdatePaymentFiveYearDeleteFlag(_service, p);
                            if (retBool)
                            {
                                //now delete the payment
                                retBool = DeletePayment(_service, p);
                                if (!retBool)
                                {
                                    Console.WriteLine("Failed to delete payment");
                                    Log.Info("Failed to delete payment");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Failed to update payment five year delete flag");
                                Log.Info("Failed to update payment five year delete flag");
                            }

                        }//end of Payment foreach loop

                        payments.Entities.Clear();


                        //Then set the application measure five year delete flag to true
                        retBool = UpdateApplicationMeasureFiveYearDeleteFlag(_service, am);
                        if (retBool)
                        {
                            //delete the application measure
                            retBool = DeleteApplicationMeasure(_service, am);
                            if (!retBool)
                            {
                                Console.WriteLine("Failed to delete application measure");
                                Log.Info("Failed to delete application measure");
                            }                            
                        }
                        else
                        {
                            Console.WriteLine("Failed to update application measure five year delete flag");
                            Log.Info("Failed to update pplication measure five year delete flag");
                        }


                    }//end of applicationMeasures foreach loop

                    //verify that no app measures remain before deleting the application
                    retValue = GetAppMeasureCountByApplication((Guid)ap.Attributes["seera_applicationid"], _service);

                    Console.WriteLine("GetAppMeasureCountByApplication= " + retValue.ToString());
                    Log.Info("GetAppMeasureCountByApplication= " + retValue.ToString());

                    if(retValue == 0)
                    { 
                        //Then set the Application five year delete flag to true
                        retBool = UpdateApplicationFiveYearDeleteFlag(_service, ap);
                        if (retBool)
                        {
                            //delete the application
                            retBool = DeleteApplication(_service, ap);
                            if (!retBool)
                            {
                                Console.WriteLine("Failed to delete application");
                                Log.Info("Failed to delete application");
                            }     
                        }
                        else
                        {
                            Console.WriteLine("Failed to update application five year delete flag");
                            Log.Info("Failed to update application five year delete flag");
                        }
                    
                    }//application has app measures < 5 years old so skip the delete.

                }//end of application foreach loop


                //Loop through the customer, provider, contractor and sub-contractor accounts that are older than 30 days and check for  
                //child cases, opportunities and leads that are 5 years old or older
                Accounts = GetAccountsGreaterThanThirtyDaysOld(_service);
                foreach (Entity account in Accounts.Entities)
                {
                    //debug
                    Console.WriteLine(" ");
                    Console.WriteLine("Account = " + account.Attributes[Account.AccountName]);
                    Log.Info(" ");
                    Log.Info("Account = " + account.Attributes[Account.AccountName]);

                    //Get the collection of cases for the current account with a created on date >= 5 years ago
                    Cases = GetFiveYearOldCasesByAccountID((Guid)account.Attributes["accountid"], _service);
                    foreach (Entity c in Cases.Entities)
                    {
                        //debug
                        Console.WriteLine("Case = " + c.Attributes[Case.CaseNumber]);
                        Log.Info("Case = " + c.Attributes[Case.CaseNumber]);

                        //set the case 5 year delete flag to true
                        retBool = UpdateCaseFiveYearDeleteFlag(_service, c);
                        if (retBool)
                        {
                            //delete the case record from CRM
                            retBool = DeleteCase(_service, c);
                            if (!retBool)
                            {
                                Console.WriteLine("Failed to delete case");
                                Log.Info("Failed to delete case");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Failed to update case five year delete flag");
                            Log.Info("Failed to update case five year delete flag");
                        }

                    }//end of case foreach loop

                    Cases.Entities.Clear();

                    //Get the collection of Opportunities for the current account with a created on date >= 5 years ago
                    Opportunities = GetFiveYearOldOpportunitiesByAccountID((Guid)account.Attributes["accountid"], _service);
                    foreach (Entity opportunity in Opportunities.Entities)
                    {
                        //debug
                        Console.WriteLine("Opportunity = " + opportunity.Attributes[Opportunity.OpportunityName]);
                        Log.Info("Opportunity = " + opportunity.Attributes[Opportunity.OpportunityName]);

                        //set the opportunity 5 year delete flag to true
                        retBool = UpdateOpportunityFiveYearDeleteFlag(_service, opportunity);
                        if (retBool)
                        {
                            //delete the opportunity record from CRM
                            retBool = DeleteOpportunity(_service, opportunity);
                            if (!retBool)
                            {
                                Console.WriteLine("Failed to delete opportunity");
                                Log.Info("Failed to delete opportunity");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Failed to update opportunity five year delete flag");
                            Log.Info("Failed to update opportunity five year delete flag");
                        }

                    }//end of opportunity foreach loop

                    Opportunities.Entities.Clear();

                    //Get the collection of leads for the current account with a created on date >= 5 years ago                 
                    Leads = GetFiveYearOldLeadsByAccountID((Guid)account.Attributes["accountid"], _service);
                    foreach (Entity lead in Leads.Entities)
                    {
                        //debug
                        Console.WriteLine("Lead = " + lead.Attributes[Lead.Subject]);
                        Log.Info("Lead = " + lead.Attributes[Lead.Subject]);

                        //set the lead 5 year delete flag to true
                        retBool = UpdateLeadFiveYearDeleteFlag(_service, lead);
                        if (retBool)
                        {
                            //delete the lead record from CRM
                            retBool = DeleteLead(_service, lead);
                            if (!retBool)
                            {
                                Console.WriteLine("Failed to delete lead");
                                Log.Info("Failed to delete lead");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Failed to update lead five year delete flag");
                            Log.Info("Failed to update lead five year delete flag");
                        }
                    }//end of lead foreach loop

                    Leads.Entities.Clear();

                    //Now that everything except the contacts have been marked and then deleted - see if anything is left on the accounts

                    //Check for Focus Invoices
                    keeperFocusInvoiceCheck = GetKeeperFocusInvoiceCountByAccountID((Guid)account.Attributes["accountid"], _service);

                    //debug
                    Console.WriteLine("keeperFocusInvoiceCheck = " + keeperFocusInvoiceCheck.ToString());
                    Log.Info("keeperFocusInvoiceCheck = " + keeperFocusInvoiceCheck.ToString());

                    //Check for Application Measures
                    keeperApplicationMeasureCheck = GetKeeperApplicationMeasureCountByAccountID((Guid)account.Attributes["accountid"], _service);

                    //debug
                    Console.WriteLine("keeperApplicationMeasureCheck = " + keeperApplicationMeasureCheck.ToString());
                    Log.Info("keeperApplicationMeasureCheck = " + keeperApplicationMeasureCheck.ToString());

                    //Check for Applications
                    keeperApplicationCheck = GetKeeperApplicationCountByAccountID((Guid)account.Attributes["accountid"], _service);

                    //debug
                    Console.WriteLine("keeperApplicationCheck = " + keeperApplicationCheck.ToString());
                    Log.Info("keeperApplicationCheck = " + keeperApplicationCheck.ToString());

                    //Check for Cases
                    keeperCaseCheck = GetKeeperCaseCountByAccountID((Guid)account.Attributes["accountid"], _service);

                    //debug
                    Console.WriteLine("keeperCaseCheck = " + keeperCaseCheck.ToString());
                    Log.Info("keeperCaseCheck = " + keeperCaseCheck.ToString());

                    //Check for Opportunities
                    keeperOpportunityCheck = GetKeeperOpportunityCountByAccountID((Guid)account.Attributes["accountid"], _service);

                    //debug
                    Console.WriteLine("keeperOpportunityCheck = " + keeperOpportunityCheck.ToString());
                    Log.Info("keeperOpportunityCheck = " + keeperOpportunityCheck.ToString());

                    //Check for Leads
                    keeperLeadCheck = GetKeeperLeadCountByAccountID((Guid)account.Attributes["accountid"], _service);

                    //debug
                    Console.WriteLine("keeperLeadCheck = " + keeperLeadCheck.ToString());
                    Log.Info("keeperLeadCheck = " + keeperLeadCheck.ToString());


                    //If not the get rid of the documents and then delete the account.
                    if (keeperFocusInvoiceCheck == 0 && keeperApplicationCheck == 0 && keeperApplicationMeasureCheck == 0 && keeperCaseCheck == 0 && keeperOpportunityCheck == 0 && keeperLeadCheck == 0)
                    {
                        //remove the sharepoint documents (TBD)

                        //get the child contacts
                        Contacts = GetContactsByAccountID((Guid)account.Attributes["accountid"], _service);
                        foreach(Entity contact in Contacts.Entities)
                        {
                            //debug
                            Console.WriteLine("Contact = " + contact.Attributes[Contact.FullName]);
                            Log.Info("Contact = " + contact.Attributes[Contact.FullName]);

                            //set the contact 5 year delete flag to true
                            retBool = UpdateContactFiveYearDeleteFlag(_service, contact);
                            if (retBool)
                            {
                                //delete the contact record from CRM
                                retBool = DeleteContact(_service, contact);
                                if (!retBool)
                                {
                                    Console.WriteLine("Failed to delete contact");
                                    Log.Info("Failed to delete contact");
                                }
                            }
                            else
                            {
                                Console.WriteLine("Failed to update contact five year delete flag");
                                Log.Info("Failed to update contact five year delete flag");
                            }

                        }//end of contact foreach loop

                        Contacts.Entities.Clear();

                        //set the Account 5 year delete flag to true
                        retBool = UpdateAccountFiveYearDeleteFlag(_service, account);
                        if (retBool)
                        {
                            //check for a sharepoint document location - if one exists then delete it from CRM 
                            DocumentLocations = GetDocumentLocationsByAccountID((Guid)account.Attributes["accountid"], _service);
                            foreach (Entity location in DocumentLocations.Entities)
                            {
                                retBool = DeleteCRMDocumentLocation(_service, location);
                                if (retBool)
                                {
                                    //Delete the document location from Sharepoint
                                    SharePoint.DeleteDocumentLocationFromSharePoint(location.Attributes[DocumentLocation.RelativeURL].ToString());
                                    if (!retBool)
                                    {
                                        Console.WriteLine("Failed to delete document location from sharepoint for " + account.Attributes[Account.AccountName]);
                                        Log.Info("Failed to delete document location from sharepoint for " + account.Attributes[Account.AccountName]);
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Failed to delete document location from CRM for " + account.Attributes[Account.AccountName]);
                                    Log.Info("Failed to delete document location from CRM for " + account.Attributes[Account.AccountName]);
                                }
                            }

                            //debug                        
                            Console.WriteLine("REMOVING ACCOUNT " + account.Attributes[Account.AccountName]);
                            Console.WriteLine(" ");
                            Console.WriteLine(" ");
                            Log.Info("REMOVING ACCOUNT " + account.Attributes[Account.AccountName]);
                            Log.Info(" ");
                            Log.Info(" ");

                            //delete the account record from CRM
                            retBool = DeleteAccount(_service, account);
                            if (!retBool)
                            {
                                Console.WriteLine("Failed to delete account");
                                Log.Info("Failed to delete account");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Failed to update account five year delete flag");
                            Log.Info("Failed to update account five year delete flag");
                        }

                    }


                }//end of account foreach loop 

                //Console.Read();

            }
        }


        #region FUNCTIONS
        private int GetKeeperFocusInvoiceCountByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            int retValue = 0;

            //get a count of any focus invoices that are less than 5 years old (FiveYearDelete field set to false or null)

            EntityCollection FocusInvoices = new EntityCollection();

            ColumnSet focusInvoiceFields = new ColumnSet();
            focusInvoiceFields.AddColumn(FocusInvoice.FocusInvoicePrimaryKey);

            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.Conditions.Add(new ConditionExpression(FocusInvoice.FocusInvoiceAccountID, ConditionOperator.Equal, inAccountID));
            filter.Conditions.Add(new ConditionExpression(FocusInvoice.FiveYearDelete, ConditionOperator.NotEqual, true));

            QueryExpression query = new QueryExpression(FocusInvoice.EntityName);
            query.ColumnSet = focusInvoiceFields;
            query.Criteria = filter;

            try
            {
                FocusInvoices = inOrgService.RetrieveMultiple(query);
                if (FocusInvoices.Entities.Count > 0)
                {
                    retValue = FocusInvoices.Entities.Count;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetKeeperFocusInvoiceCountByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetKeeperFocusInvoiceCountByAccountID. Message=" + ex.Message);
            }

            return retValue;
        }


        private int GetKeeperApplicationCountByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            int retValue = 0;

            EntityCollection Applications = new EntityCollection();

            ColumnSet applicationFields = new ColumnSet();
            applicationFields.AddColumn(Application.ApplicationPrimaryKey);

            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.Conditions.Add(new ConditionExpression(Application.Customer, ConditionOperator.Equal, inAccountID));
            filter.Conditions.Add(new ConditionExpression(Application.FiveYearDelete, ConditionOperator.NotEqual, true));

            QueryExpression query = new QueryExpression(Application.EntityName);
            query.ColumnSet = applicationFields;
            query.Criteria = filter;

            try
            {
                Applications = inOrgService.RetrieveMultiple(query);
                if (Applications.Entities.Count > 0)
                {
                    retValue = Applications.Entities.Count;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetKeeperApplicationCountByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetKeeperApplicationCountByAccountID. Message=" + ex.Message);
            }

            return retValue;
        }


        private int GetKeeperApplicationMeasureCountByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            int retValue = 0;

            EntityCollection ApplicationMeasures = new EntityCollection();

            ColumnSet applicationMeasureFields = new ColumnSet();
            applicationMeasureFields.AddColumn(ApplicationMeasure.ApplicationMeasurePrimaryKey);

            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.Conditions.Add(new ConditionExpression(ApplicationMeasure.ApplicationMeasureCustomerID, ConditionOperator.Equal, inAccountID));
            filter.Conditions.Add(new ConditionExpression(ApplicationMeasure.FiveYearDelete, ConditionOperator.NotEqual, true));

            QueryExpression query = new QueryExpression(ApplicationMeasure.EntityName);
            query.ColumnSet = applicationMeasureFields;
            query.Criteria = filter;

            try
            {
                ApplicationMeasures = inOrgService.RetrieveMultiple(query);
                if (ApplicationMeasures.Entities.Count > 0)
                {
                    retValue = ApplicationMeasures.Entities.Count;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetKeeperApplicationMeasureCountByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetKeeperApplicationMeasureCountByAccountID. Message=" + ex.Message);
            }

            return retValue;
        }

       

        private int GetKeeperCaseCountByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            int retValue = 0;

            EntityCollection Cases = new EntityCollection();

            ColumnSet caseFields = new ColumnSet();
            caseFields.AddColumn(Case.CasePrimaryKey);

            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.Conditions.Add(new ConditionExpression(Case.ParentAccountID, ConditionOperator.Equal, inAccountID));
            filter.Conditions.Add(new ConditionExpression(Case.FiveYearDelete, ConditionOperator.NotEqual, true));

            QueryExpression query = new QueryExpression(Case.EntityName);
            query.ColumnSet = caseFields;
            query.Criteria = filter;

            try
            {
                Cases = inOrgService.RetrieveMultiple(query);
                if (Cases.Entities.Count > 0)
                {
                    retValue = Cases.Entities.Count;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetKeeperCaseCountByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetKeeperCaseCountByAccountID. Message=" + ex.Message);
            }

            return retValue;

        }

        private int GetKeeperOpportunityCountByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            int retValue = 0;

            EntityCollection Opportunities = new EntityCollection();

            ColumnSet opportunityFields = new ColumnSet();
            opportunityFields.AddColumn(Opportunity.OpportunityPrimaryKey);

            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.Conditions.Add(new ConditionExpression(Opportunity.ParentAccountID, ConditionOperator.Equal, inAccountID));
            filter.Conditions.Add(new ConditionExpression(Opportunity.FiveYearDelete, ConditionOperator.NotEqual, true));

            QueryExpression query = new QueryExpression(Opportunity.EntityName);
            query.ColumnSet = opportunityFields;
            query.Criteria = filter;

            try
            {
                Opportunities = inOrgService.RetrieveMultiple(query);
                if (Opportunities.Entities.Count > 0)
                {
                    retValue = Opportunities.Entities.Count;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetKeeperOpportunityCountByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetKeeperOpportunityCountByAccountID. Message=" + ex.Message);
            }

            return retValue;

        }

        private int GetKeeperLeadCountByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            int retValue = 0;

            EntityCollection Leads = new EntityCollection();

            ColumnSet leadFields = new ColumnSet();
            leadFields.AddColumn(Lead.LeadPrimaryKey);

            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.Conditions.Add(new ConditionExpression(Lead.ParentAccountID, ConditionOperator.Equal, inAccountID));
            filter.Conditions.Add(new ConditionExpression(Lead.FiveYearDelete, ConditionOperator.NotEqual, true));

            QueryExpression query = new QueryExpression(Lead.EntityName);
            query.ColumnSet = leadFields;
            query.Criteria = filter;

            try
            {
                Leads = inOrgService.RetrieveMultiple(query);
                if (Leads.Entities.Count > 0)
                {
                    retValue = Leads.Entities.Count;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetKeeperLeadCountByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetKeeperLeadCountByAccountID. Message=" + ex.Message);
            }

            return retValue;

        }


        private int GetAppMeasureCountByApplication(Guid inApplicationID, OrganizationService inOrgService)
        {
            int retValue = 0;

            EntityCollection ApplicationMeasures = new EntityCollection();

            ColumnSet applicationMeasureFields = new ColumnSet();
            applicationMeasureFields.AddColumn(ApplicationMeasure.ApplicationMeasurePrimaryKey);

            ConditionExpression cond = new ConditionExpression(ApplicationMeasure.ApplicationMeasureApplicationID, ConditionOperator.Equal, inApplicationID);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond);

            QueryExpression query = new QueryExpression(ApplicationMeasure.EntityName);
            query.ColumnSet = applicationMeasureFields;
            query.Criteria = filter;

            try
            {
                ApplicationMeasures = inOrgService.RetrieveMultiple(query);
                if (ApplicationMeasures.Entities.Count > 0)
                {
                    retValue = ApplicationMeasures.Entities.Count;
                }
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetAppMeasureCountByApplication. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetAppMeasureCountByApplication. Message=" + ex.Message);
            }

            return retValue;

        }

        private EntityCollection GetInvoiceDetailsByFocusInvoiceID(Guid inFocusInvoiceID, OrganizationService inOrgService)
        {
            EntityCollection InvoiceDetails = new EntityCollection();

            ColumnSet invoiceDetailFields = new ColumnSet();
            invoiceDetailFields.AddColumn(FocusInvoiceDetail.FocusInvoiceDetailPrimaryKey);
            invoiceDetailFields.AddColumn(FocusInvoiceDetail.ParentFocusInvoiceID);
            invoiceDetailFields.AddColumn(FocusInvoiceDetail.FiveYearDelete);
            invoiceDetailFields.AddColumn(FocusInvoiceDetail.ExpenditureDate);
            invoiceDetailFields.AddColumn(FocusInvoiceDetail.InvoiceDetailID);
            

            ConditionExpression cond = new ConditionExpression(FocusInvoiceDetail.ParentFocusInvoiceID, ConditionOperator.Equal, inFocusInvoiceID);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond);

            QueryExpression query = new QueryExpression(FocusInvoiceDetail.EntityName);
            query.ColumnSet = invoiceDetailFields;
            query.Criteria = filter;

            try
            {
                InvoiceDetails = inOrgService.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetInvoiceDetailsByFocusInvoiceID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetInvoiceDetailsByFocusInvoiceID. Message=" + ex.Message);
            }

            return InvoiceDetails;
        }


        private EntityCollection GetApplicationMeasuresByApplicationID(Guid inApplicationID, OrganizationService inOrgService)
        {

            EntityCollection ApplicationMeasures = new EntityCollection();

            ColumnSet applicationMeasureFields = new ColumnSet();
            applicationMeasureFields.AddColumn(ApplicationMeasure.ApplicationMeasurePrimaryKey);
            applicationMeasureFields.AddColumn(ApplicationMeasure.ApplicationMeasureApplicationID);
            applicationMeasureFields.AddColumn(ApplicationMeasure.FiveYearDelete);
            applicationMeasureFields.AddColumn(ApplicationMeasure.DateOfStatusChangeToPaid);
            applicationMeasureFields.AddColumn(ApplicationMeasure.ApplicationMeasureID);
            

            ConditionExpression cond = new ConditionExpression(ApplicationMeasure.ApplicationMeasureApplicationID, ConditionOperator.Equal, inApplicationID);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond);

            QueryExpression query = new QueryExpression(ApplicationMeasure.EntityName);
            query.ColumnSet = applicationMeasureFields;
            query.Criteria = filter;

            try
            {
                ApplicationMeasures = inOrgService.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetApplicationMeasuresByApplicationID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetApplicationMeasuresByApplicationID. Message=" + ex.Message);
            }

            return ApplicationMeasures;
        }


        private EntityCollection GetPaymentsByInvoiceDetailID(Guid inInvoiceDetailID, OrganizationService inOrgService)
        {
            
            EntityCollection Payments = new EntityCollection();                      
            ColumnSet paymentFields = new ColumnSet();
            paymentFields.AddColumn(Payment.PaymentPrimaryKey);
            paymentFields.AddColumn(Payment.ParentInvoiceDetailID);
            paymentFields.AddColumn(Payment.PaymentID);
            paymentFields.AddColumn(Payment.FiveYearDelete);

            ConditionExpression cond = new ConditionExpression(Payment.ParentInvoiceDetailID, ConditionOperator.Equal, inInvoiceDetailID);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond);

            QueryExpression query = new QueryExpression(Payment.EntityName);
            query.ColumnSet = paymentFields;
            query.Criteria = filter;

            try
            {
                Payments = inOrgService.RetrieveMultiple(query);               
            }
            catch(Exception ex)
            {
                Log.Error("ERROR in GetPaymentsByFocusInvoiceID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetPaymentsByFocusInvoiceID. Message=" + ex.Message);
            }

            return Payments;
        }


        private EntityCollection GetPaymentsByApplicationMeasureID(Guid inApplicationMeasureID, OrganizationService inOrgService)
        {
            EntityCollection Payments = new EntityCollection();
            ColumnSet paymentFields = new ColumnSet();
            paymentFields.AddColumn(Payment.PaymentPrimaryKey);
            paymentFields.AddColumn(Payment.ParentInvoiceDetailID);
            paymentFields.AddColumn(Payment.PaymentID);
            paymentFields.AddColumn(Payment.FiveYearDelete);

            ConditionExpression cond = new ConditionExpression(Payment.ParentApplicationMeasureID, ConditionOperator.Equal, inApplicationMeasureID);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond);

            QueryExpression query = new QueryExpression(Payment.EntityName);
            query.ColumnSet = paymentFields;
            query.Criteria = filter;

            try
            {
                Payments = inOrgService.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetPaymentsByApplicationMeasureID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetPaymentsByApplicationMeasureID. Message=" + ex.Message);
            }

            return Payments;
        }

        private EntityCollection GetFiveYearOldCasesByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            DateTime current = DateTime.Now;
            int day = current.Day;
            int month = current.Month;
            int year = current.Year;
            int removeYear = year - 5;

            EntityCollection Cases = new EntityCollection();
            DateTime fiveYearsAgo = new DateTime(removeYear, month, day);

            ColumnSet caseFields = new ColumnSet();
            caseFields.AddColumn(Case.CasePrimaryKey);
            caseFields.AddColumn(Case.ParentAccountID);
            caseFields.AddColumn(Case.FiveYearDelete);
            caseFields.AddColumn(Case.CreatedOn);
            caseFields.AddColumn(Case.CaseNumber);
            

            ConditionExpression cond1 = new ConditionExpression(Case.ParentAccountID, ConditionOperator.Equal, inAccountID);
            ConditionExpression cond2 = new ConditionExpression(Case.CreatedOn, ConditionOperator.LessEqual, fiveYearsAgo);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond1);
            filter.AddCondition(cond2);

            QueryExpression query = new QueryExpression(Case.EntityName);
            query.ColumnSet = caseFields;
            query.Criteria = filter;

            try
            {
                Cases = inOrgService.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetCasesByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetCasesByAccountID. Message=" + ex.Message);
            }

            return Cases;

        }

        private EntityCollection GetFiveYearOldOpportunitiesByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            DateTime current = DateTime.Now;
            int day = current.Day;
            int month = current.Month;
            int year = current.Year;
            int removeYear = year - 5;

            EntityCollection Opportunities = new EntityCollection();
            DateTime fiveYearsAgo = new DateTime(removeYear, month, day);

            ColumnSet opportunityFields = new ColumnSet();
            opportunityFields.AddColumn(Opportunity.OpportunityPrimaryKey);
            opportunityFields.AddColumn(Opportunity.ParentAccountID);
            opportunityFields.AddColumn(Opportunity.FiveYearDelete);
            opportunityFields.AddColumn(Opportunity.CreatedOn);
            opportunityFields.AddColumn(Opportunity.OpportunityName);
            

            ConditionExpression cond1 = new ConditionExpression(Opportunity.ParentAccountID, ConditionOperator.Equal, inAccountID);
            ConditionExpression cond2 = new ConditionExpression(Opportunity.CreatedOn, ConditionOperator.LessEqual, fiveYearsAgo);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond1);
            filter.AddCondition(cond2);

            QueryExpression query = new QueryExpression(Opportunity.EntityName);
            query.ColumnSet = opportunityFields;
            query.Criteria = filter;

            try
            {
                Opportunities = inOrgService.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetOpportunitiesByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetOpportunitiesByAccountID. Message=" + ex.Message);
            }

            return Opportunities;

        }

        private EntityCollection GetFiveYearOldLeadsByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            DateTime current = DateTime.Now;
            int day = current.Day;
            int month = current.Month;
            int year = current.Year;
            int removeYear = year - 5;

            EntityCollection Leads = new EntityCollection();
            DateTime fiveYearsAgo = new DateTime(removeYear, month, day);

            ColumnSet leadFields = new ColumnSet();
            leadFields.AddColumn(Lead.LeadPrimaryKey);
            leadFields.AddColumn(Lead.ParentAccountID);
            leadFields.AddColumn(Lead.FiveYearDelete);
            leadFields.AddColumn(Lead.CreatedOn);
            leadFields.AddColumn(Lead.Subject);
            

            ConditionExpression cond1 = new ConditionExpression(Lead.ParentAccountID, ConditionOperator.Equal, inAccountID);
            ConditionExpression cond2 = new ConditionExpression(Lead.CreatedOn, ConditionOperator.LessEqual, fiveYearsAgo);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond1);
            filter.AddCondition(cond2);

            QueryExpression query = new QueryExpression(Lead.EntityName);
            query.ColumnSet = leadFields;
            query.Criteria = filter;

            try
            {
                Leads = inOrgService.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetLeadsByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetLeadsByAccountID. Message=" + ex.Message);
            }

            return Leads;

        }


        private EntityCollection GetContactsByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {

            EntityCollection Contacts = new EntityCollection();
            
            ColumnSet contactFields = new ColumnSet();
            contactFields.AddColumn(Contact.ContactPrimaryKey);
            contactFields.AddColumn(Contact.ParentAccountID);
            contactFields.AddColumn(Contact.FiveYearDelete);
            contactFields.AddColumn(Contact.CreatedOn);
            contactFields.AddColumn(Contact.FullName);
            

            ConditionExpression cond = new ConditionExpression(Contact.ParentAccountID, ConditionOperator.Equal, inAccountID);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond);

            QueryExpression query = new QueryExpression(Contact.EntityName);
            query.ColumnSet = contactFields;
            query.Criteria = filter;

            try
            {
                Contacts = inOrgService.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetContactsByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetContactsByAccountID. Message=" + ex.Message);
            }

            return Contacts;

        }

        private static EntityCollection GetDocumentLocationsByAccountID(Guid inAccountID, OrganizationService inOrgService)
        {
            EntityCollection DocumentLocations = new EntityCollection();

            ColumnSet documentLocationFields = new ColumnSet();
            documentLocationFields.AddColumn(DocumentLocation.SharePointDocumentLocationPrimaryKey);
            documentLocationFields.AddColumn(DocumentLocation.Name);
            documentLocationFields.AddColumn(DocumentLocation.ParentSiteOrLocation);
            documentLocationFields.AddColumn(DocumentLocation.AbsoluteURL);
            documentLocationFields.AddColumn(DocumentLocation.RelativeURL);


            ConditionExpression cond = new ConditionExpression(DocumentLocation.Regarding, ConditionOperator.Equal, inAccountID);
            FilterExpression filter = new FilterExpression(LogicalOperator.And);
            filter.AddCondition(cond);

            QueryExpression query = new QueryExpression(DocumentLocation.EntityName);
            query.ColumnSet = documentLocationFields;
            query.Criteria = filter;

            try
            {
                DocumentLocations = inOrgService.RetrieveMultiple(query);
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetDocumentLocationsByAccountID. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetDocumentLocationsByAccountID. Message=" + ex.Message);
            }

            return DocumentLocations;
        }



        private EntityCollection GetApplicationsGreaterThanFiveYearsOld(OrganizationService service)
        {
            //only get application measures that were paid over 5 years ago.
            DateTime current = DateTime.Now;
            int day = current.Day;
            int month = current.Month;
            int year = current.Year;
            int removeYear = year - 5;

            EntityCollection Applications = new EntityCollection();
            DateTime fiveYearsAgo = new DateTime(removeYear, month, day);

            QueryExpression qe = new QueryExpression(Application.EntityName);
            qe.Distinct = true;
            qe.NoLock = true;
            qe.ColumnSet = new ColumnSet();
            qe.ColumnSet.AddColumn(Application.ApplicationPrimaryKey);
            qe.ColumnSet.AddColumn(Application.FocusApplicationName);
            qe.ColumnSet.AddColumn(Application.ApplicationName);
            qe.ColumnSet.AddColumn(Application.ApplicationID);
            qe.ColumnSet.AddColumn(Application.FiveYearDelete);
            qe.ColumnSet.AddColumn(Application.Customer);
            qe.ColumnSet.AddColumn(Application.CreatedOn);

            qe.LinkEntities.Add(new LinkEntity(Application.EntityName, ApplicationMeasure.EntityName, Application.ApplicationPrimaryKey, ApplicationMeasure.ApplicationMeasureApplicationID, JoinOperator.Inner));
            qe.LinkEntities[0].LinkCriteria.AddCondition(ApplicationMeasure.DateOfStatusChangeToPaid, ConditionOperator.LessEqual, fiveYearsAgo);
            qe.LinkEntities[0].EntityAlias = "am";

            qe.Criteria = new FilterExpression();
            qe.Criteria.AddCondition(Application.FiveYearDelete, ConditionOperator.NotEqual, true);
            


            try
            {
                Applications = service.RetrieveMultiple(qe);
            }
            catch (Exception GetApplicationsGreaterThanFiveYearsOldEX)
            {
                Log.Error("ERROR in GetApplicationsGreaterThanFiveYearsOld. Message=" + GetApplicationsGreaterThanFiveYearsOldEX.Message + " " + GetApplicationsGreaterThanFiveYearsOldEX.InnerException + " " + GetApplicationsGreaterThanFiveYearsOldEX.StackTrace);
                Console.WriteLine("ERROR in GetApplicationsGreaterThanFiveYearsOld. Message=" + GetApplicationsGreaterThanFiveYearsOldEX.Message);
            }

            return Applications;
        }



        private EntityCollection GetFocusInvoicesGreaterThanFiveYearsOld(OrganizationService inService)
        {
          
            //select i.seera_accountidname,i.seera_invoicenumber,
            //seera_invoicedetailsid,seera_expendituredate,
            //seera_invoicenumbername,seera_programsname 
            //from seera_invoice i
            //join seera_invoicedetails id on i.seera_invoiceid = id.seera_invoicenumber
            //where id.seera_expendituredate is not null
            //order by seera_invoicenumber

            //only get focus invoices that were paid over 5 years ago.
            DateTime current = DateTime.Now;
            int day = current.Day;
            int month = current.Month;
            int year = current.Year;
            int removeYear = year - 5;

            EntityCollection FocusInvoices = new EntityCollection();
            DateTime fiveYearsAgo = new DateTime(removeYear, month, day);

            QueryExpression qe = new QueryExpression(FocusInvoice.EntityName);
            qe.Distinct = true;
            qe.NoLock = true;
            qe.ColumnSet = new ColumnSet();
            qe.ColumnSet.Columns.Add(FocusInvoice.FocusInvoicePrimaryKey);
            qe.ColumnSet.Columns.Add(FocusInvoice.FiveYearDelete);
            qe.ColumnSet.Columns.Add(FocusInvoice.CreatedOn);
            qe.ColumnSet.Columns.Add(FocusInvoice.FocusInvoiceID);
            
            qe.LinkEntities.Add(new LinkEntity(FocusInvoice.EntityName, FocusInvoiceDetail.EntityName,FocusInvoice.FocusInvoicePrimaryKey, FocusInvoiceDetail.ParentFocusInvoiceID, JoinOperator.Inner));
            qe.LinkEntities[0].LinkCriteria.AddCondition(FocusInvoiceDetail.ExpenditureDate, ConditionOperator.LessEqual, fiveYearsAgo);
            qe.LinkEntities[0].EntityAlias = "fid";

            qe.Criteria = new FilterExpression();
            qe.Criteria.AddCondition(FocusInvoice.FiveYearDelete, ConditionOperator.NotEqual, true);
            
            try
            {
                FocusInvoices = inService.RetrieveMultiple(qe);
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in GetFocusInvoicesGreaterThanFiveYearsOld. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in GetFocusInvoicesGreaterThanFiveYearsOld. Message=" + ex.Message);
            }

            return FocusInvoices;
        }


        private EntityCollection GetAccountsGreaterThanThirtyDaysOld(OrganizationService inService)
        {
            //Get all customer,provider,contractor and sub-contractor accounts that are at least 30 days old
            DateTime current = DateTime.Now;
            DateTime ThirtyDaysAgo = current.AddDays(-30);

            EntityCollection Accounts = new EntityCollection();
            
            QueryExpression qe = new QueryExpression(Account.EntityName);
            qe.ColumnSet = new ColumnSet();
            qe.ColumnSet.AddColumn(Account.AccountPrimaryKey);
            qe.ColumnSet.AddColumn(Account.AccountName);
            qe.ColumnSet.AddColumn(Account.FiveYearDelete);
            qe.ColumnSet.AddColumn(Account.AccountTypeCustomer);
            qe.ColumnSet.AddColumn(Account.AccountTypeProvider);
            qe.ColumnSet.AddColumn(Account.AccountTypeUtility);
            qe.ColumnSet.AddColumn(Account.AccountTypeSubContractor);
            qe.ColumnSet.AddColumn(Account.AccountTypeContractor);
            qe.ColumnSet.AddColumn(Account.CreatedOn);    

            qe.Criteria = new FilterExpression();
            qe.Criteria.FilterOperator = LogicalOperator.And;
 
            //make sure the account is over 30 days old and then exclude the account type "utility"
            FilterExpression filter1 = qe.Criteria.AddFilter(LogicalOperator.And);
            filter1.Conditions.Add(new ConditionExpression(Account.CreatedOn, ConditionOperator.LessEqual, ThirtyDaysAgo));
            filter1.Conditions.Add(new ConditionExpression(Account.AccountTypeUtility, ConditionOperator.Equal, false));
          
            try
            {
                Accounts = inService.RetrieveMultiple(qe);
            }
            catch (Exception GetAccountsGreaterThanThirtyDaysOldEX)
            {
                Log.Error("ERROR in GetAccountsGreaterThanThirtyDaysOldEX. Message=" + GetAccountsGreaterThanThirtyDaysOldEX.Message + " " + GetAccountsGreaterThanThirtyDaysOldEX.InnerException + " " + GetAccountsGreaterThanThirtyDaysOldEX.StackTrace);
                Console.WriteLine("ERROR in GetAccountsGreaterThanThirtyDaysOldEX. Message=" + GetAccountsGreaterThanThirtyDaysOldEX.Message);
            }

            return Accounts;

        }


        private bool UpdatePaymentFiveYearDeleteFlag(OrganizationService inService, Entity inPayment)
        {
            bool retBool = false;

            try
            {
                inPayment.Attributes[Payment.FiveYearDelete] = true;
                inService.Update(inPayment);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inPayment.Attributes[Payment.PaymentID] + " Payment.FiveYearDelete successfully updated to true");
                Log.Info(inPayment.Attributes[Payment.PaymentID] + " Payment.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdatePaymentFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdatePaymentFiveYearDeleteFlag. Message=" + ex.Message);
            }

            return retBool;
        }

        
        private bool UpdateFocusInvoiceDetailFiveYearDeleteFlag(OrganizationService service, Entity inFocusInvoiceDetail)
        {
            bool retBool = false;

            try
            {
                inFocusInvoiceDetail.Attributes[FocusInvoiceDetail.FiveYearDelete] = true;
                service.Update(inFocusInvoiceDetail);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inFocusInvoiceDetail.Attributes[FocusInvoiceDetail.InvoiceDetailID] + " FocusInvoiceDetail.FiveYearDelete successfully updated to true");
                Log.Info(inFocusInvoiceDetail.Attributes[FocusInvoiceDetail.InvoiceDetailID] + " FocusInvoiceDetail.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdateFocusInvoiceDetailFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdateFocusInvoiceDetailFiveYearDeleteFlag. Message=" + ex.Message);
            }

            return retBool;
        }

     
        private bool UpdateFocusInvoiceFiveYearDeleteFlag(OrganizationService service, Entity inFocusInvoice)
        {
            bool retBool = false;

            try
            {
                inFocusInvoice.Attributes[FocusInvoice.FiveYearDelete] = true;
                service.Update(inFocusInvoice);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inFocusInvoice.Attributes[FocusInvoice.FocusInvoiceID] + " FocusInvoice.FiveYearDelete successfully updated to true");
                Log.Info(inFocusInvoice.Attributes[FocusInvoice.FocusInvoiceID] + " FocusInvoice.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdateFocusInvoiceFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdateFocusInvoiceFiveYearDeleteFlag. Message=" + ex.Message);
            }

            return retBool;
        }


        private bool UpdateApplicationMeasureFiveYearDeleteFlag(OrganizationService service, Entity inApplicationMeasure)
        {
            bool retBool = false;

            try
            {
                inApplicationMeasure.Attributes[ApplicationMeasure.FiveYearDelete] = true;
                service.Update(inApplicationMeasure);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inApplicationMeasure.Attributes[ApplicationMeasure.ApplicationMeasureID] + " ApplicationMeasure.FiveYearDelete successfully updated to true");
                Log.Info(inApplicationMeasure.Attributes[ApplicationMeasure.ApplicationMeasureID] + " ApplicationMeasure.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdateApplicationMeasureFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdateApplicationMeasureFiveYearDeleteFlag. Message=" + ex.Message);
            }

            return retBool;
        }


        private bool UpdateApplicationFiveYearDeleteFlag(OrganizationService service, Entity inApplication)
        {
            bool retBool = false;

            try
            {
                inApplication.Attributes[Application.FiveYearDelete] = true;
                service.Update(inApplication);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inApplication.Attributes[Application.ApplicationName] + " Application.FiveYearDelete successfully updated to true");
                Log.Info(inApplication.Attributes[Application.ApplicationName] + " Application.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdateApplicationFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdateApplicationFiveYearDeleteFlag. Message=" + ex.Message);
            }

            return retBool;
        }


        private bool UpdateAccountFiveYearDeleteFlag(OrganizationService service, Entity inAccount)
        {
            bool retBool = false;

            try
            {
                inAccount.Attributes[Account.FiveYearDelete] = true;
                service.Update(inAccount);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inAccount.Attributes[Account.AccountName] + " Account.FiveYearDelete successfully updated to true");
                Log.Info(inAccount.Attributes[Account.AccountName] + " Account.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdateAccountFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdateAccountFiveYearDeleteFlag. Message=" + ex.Message);
            }


            return retBool;
        }

        private bool UpdateCaseFiveYearDeleteFlag(OrganizationService service, Entity inCase)
        {
            bool retBool = false;

            try
            {
                inCase.Attributes[Case.FiveYearDelete] = true;
                service.Update(inCase);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inCase.Attributes[Case.CaseNumber] + " Case.FiveYearDelete successfully updated to true");
                Log.Info(inCase.Attributes[Case.CaseNumber] + " Case.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdateCaseFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdateCaseFiveYearDeleteFlag. Message=" + ex.Message);
            }


            return retBool;
        }


        private bool UpdateOpportunityFiveYearDeleteFlag(OrganizationService service, Entity inOpportunity)
        {
            bool retBool = false;

            try
            {
                inOpportunity.Attributes[Opportunity.FiveYearDelete] = true;
                service.Update(inOpportunity);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inOpportunity.Attributes[Opportunity.OpportunityName] + " Opportunity.FiveYearDelete successfully updated to true");
                Log.Info(inOpportunity.Attributes[Opportunity.OpportunityName] + " Opportunity.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdateOpportunityFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdateOpportunityFiveYearDeleteFlag. Message=" + ex.Message);
            }

            return retBool;
        }


        private bool UpdateLeadFiveYearDeleteFlag(OrganizationService service, Entity inLead)
        {
            bool retBool = false;

            try
            {
                inLead.Attributes[Lead.FiveYearDelete] = true;
                service.Update(inLead);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inLead.Attributes[Lead.Subject] + " Lead.FiveYearDelete successfully updated to true");
                Log.Info(inLead.Attributes[Lead.Subject] + " Lead.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdateLeadFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdateLeadFiveYearDeleteFlag. Message=" + ex.Message);
            }

            return retBool;
        }


        private bool UpdateContactFiveYearDeleteFlag(OrganizationService service, Entity inContact)
        {
            bool retBool = false;

            try
            {
                inContact.Attributes[Contact.FiveYearDelete] = true;
                service.Update(inContact);
                retBool = true;

                //troubleshooting only
                Console.WriteLine(inContact.Attributes[Contact.FullName] + " Contact.FiveYearDelete successfully updated to true");
                Log.Info(inContact.Attributes[Contact.FullName] + " Contact.FiveYearDelete successfully updated to true");
            }
            catch (Exception ex)
            {
                Log.Error("ERROR in UpdateContactFiveYearDeleteFlag. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                Console.WriteLine("ERROR in UpdateContactFiveYearDeleteFlag. Message=" + ex.Message);
            }

            return retBool;
        }


        private bool DeletePayment(OrganizationService service, Entity inPayment)
        {
            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {
                try
                {
                    service.Delete(Payment.EntityName, (Guid)inPayment.Attributes[Payment.PaymentPrimaryKey]);
                    retBool = true;

                    Log.Error("Payment Deleted");
                    Console.WriteLine("EPayment Deleted");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeletePayment. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeletePayment. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;
        }


        private bool DeleteInvoiceDetail(OrganizationService service, Entity inInvoiceDetail)
        {
            bool retBool = false;

             string delete = ConfigurationManager.AppSettings["Delete"];

             if (delete.ToUpper() == "Y")
             {
                 try
                 {
                     service.Delete(FocusInvoiceDetail.EntityName, (Guid)inInvoiceDetail.Attributes[FocusInvoiceDetail.FocusInvoiceDetailPrimaryKey]);
                     retBool = true;

                     Log.Error("Invoice Detail Deleted");
                     Console.WriteLine("Invoice Detail Deleted");
                 }
                 catch (Exception ex)
                 {
                     Log.Error("ERROR in DeleteInvoiceDetail. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                     Console.WriteLine("ERROR in DeleteInvoiceDetail. Message=" + ex.Message);
                 }
             }
             else
             {
                 retBool = true;
             }

            return retBool;
        }


        private bool DeleteFocusInvoice(OrganizationService service, Entity inFocusInvoice)
        {
            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {

                try
                {
                    service.Delete(FocusInvoice.EntityName, (Guid)inFocusInvoice.Attributes[FocusInvoice.FocusInvoicePrimaryKey]);
                    retBool = true;

                    Log.Error("Focus Invoice Deleted");
                    Console.WriteLine("Focus Invoice Deleted");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeleteFocusInvoice. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeleteFocusInvoice. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;
        }


        private bool DeleteApplicationMeasure(OrganizationService service, Entity inApplicationMeasure)
        {
            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {
                try
                {
                    service.Delete(ApplicationMeasure.EntityName, (Guid)inApplicationMeasure.Attributes[ApplicationMeasure.ApplicationMeasurePrimaryKey]);
                    retBool = true;

                    Log.Error("Application Measure Deleted");
                    Console.WriteLine("Applciation Measure Deleted");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeleteApplicationMeasure. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeleteApplicationMeasure. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;

        }

        private bool DeleteApplication(OrganizationService service, Entity inApplication)
        {
            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {

                try
                {
                    service.Delete(Application.EntityName, (Guid)inApplication.Attributes[Application.ApplicationPrimaryKey]);
                    retBool = true;

                    Log.Error("Application Deleted");
                    Console.WriteLine("Application Deleted");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeleteApplication. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeleteApplication. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;

        }


        private bool DeleteCase(OrganizationService service, Entity inCase)
        {
            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {
                try
                {
                    service.Delete(Case.EntityName, (Guid)inCase.Attributes[Case.CasePrimaryKey]);
                    retBool = true;

                    Log.Error("Case Deleted");
                    Console.WriteLine("Case Deleted");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeleteCase. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeleteCase. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;
        }


        private bool DeleteOpportunity(OrganizationService service, Entity inOpportunity)
        {
            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {
                try
                {
                    service.Delete(Opportunity.EntityName, (Guid)inOpportunity.Attributes[Opportunity.OpportunityPrimaryKey]);
                    retBool = true;

                    Log.Error("Opportunity Deleted");
                    Console.WriteLine("Opportunity Deleted");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeleteOpportunity. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeleteOpportunity. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;
        }


        private bool DeleteLead(OrganizationService service, Entity inLead)
        {
            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {
                try
                {
                    service.Delete(Lead.EntityName, (Guid)inLead.Attributes[Lead.LeadPrimaryKey]);
                    retBool = true;

                    Log.Error("Lead Deleted");
                    Console.WriteLine("Lead Deleted");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeleteLead. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeleteLead. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;
        }


        private bool DeleteContact(OrganizationService service, Entity inContact)
        {
            bool retBool = false;

            //Contacts are only deleted if the parent account is > 30 days old and has no other child entities less than 5 years old
            //ALl contacts are deleted from the account, no matter the age, befor the account is actually deleted from CRM.

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {
                try
                {
                    service.Delete(Contact.EntityName, (Guid)inContact.Attributes[Contact.ContactPrimaryKey]);
                    retBool = true;

                    Log.Error("Contact Deleted");
                    Console.WriteLine("Contact Deleted");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeleteContact. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeleteContact. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;
        }


        private bool DeleteAccount(OrganizationService service, Entity inAccount)
        {
            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {
                try
                {
                    service.Delete(Account.EntityName, (Guid)inAccount.Attributes[Account.AccountPrimaryKey]);
                    retBool = true;

                    Log.Error("Account Deleted");
                    Console.WriteLine("Account Deleted");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeleteAccount. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeleteAccount. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;
        }


        private bool DeleteCRMDocumentLocation(OrganizationService service,Entity inDocumentLocation)
        {
            bool retBool = false;

            string delete = ConfigurationManager.AppSettings["Delete"];

            if (delete.ToUpper() == "Y")
            {
                try
                {
                    service.Delete(DocumentLocation.EntityName, (Guid)inDocumentLocation.Attributes[DocumentLocation.SharePointDocumentLocationPrimaryKey]);
                    retBool = true;
                    Log.Error("Successfully deleted CRM Document Location");
                    Console.WriteLine("Successfully deleted CRM Document Location");
                }
                catch (Exception ex)
                {
                    Log.Error("ERROR in DeleteDocumentLocation. Message=" + ex.Message + " " + ex.InnerException + " " + ex.StackTrace);
                    Console.WriteLine("ERROR in DeleteDocumentLocation. Message=" + ex.Message);
                }
            }
            else
            {
                retBool = true;
            }

            return retBool;

        }

        private bool IsRecordGreaterThanThirtyDaysOld(DateTime dateToBeEvaluated)
        {
            bool retValue = false;

            DateTime test = dateToBeEvaluated.AddDays(30);
            if(test < DateTime.Now)
            {
                retValue = true;
            }
            
            return retValue;
        }

#endregion FUNCTIONS

        //************************************************************************************************************************************
        #region Connection Code

        private static String GetServiceConfiguration()
        {
            // Get available connection strings from app.config.
            int count = ConfigurationManager.ConnectionStrings.Count;

            // Create a filter list of connection strings so that we have a list of valid
            // connection strings for Microsoft Dynamics CRM only.
            var filteredConnectionStrings = new List<KeyValuePair<String, String>>();

            for (int a = 0; a < count; a++)
            {
                if (IsValidConnectionString(ConfigurationManager.ConnectionStrings[a].ConnectionString))
                    filteredConnectionStrings.Add
                        (new KeyValuePair<string, string>
                            (ConfigurationManager.ConnectionStrings[a].Name,
                            ConfigurationManager.ConnectionStrings[a].ConnectionString));
            }

            // No valid connections strings found. Write out and error message.
            if (filteredConnectionStrings.Count == 0)
            {
                Log.Info("An app.config file containing at least one valid Microsoft Dynamics CRM connection string configuration must exist in the run-time folder.");
                //Console.WriteLine("An app.config file containing at least one valid Microsoft Dynamics CRM " +
                //"connection string configuration must exist in the run-time folder.");
                Log.Info("There are several commented out example connection strings in the provided app.config file. Uncomment one of them and modify the string according to your Microsoft Dynamics CRM installation. Then re-run the sample.");
                //Console.WriteLine("\nThere are several commented out example connection strings in " +
                //  "the provided app.config file. Uncomment one of them and modify the string according " +
                //"to your Microsoft Dynamics CRM installation. Then re-run the sample.");
                return null;
            }

            // If one valid connection string is found, use that.
            if (filteredConnectionStrings.Count == 1)
            {
                string url = filteredConnectionStrings[0].Value;
                string[] temp = url.Split(';');
                Log.Info("Connecting to " + temp[0].Substring(4));
                Console.WriteLine("Connecting to " + temp[0].Substring(4));
                return filteredConnectionStrings[0].Value;
            }

            return null;
        }

        /// <summary>
        /// Verifies if a connection string is valid for Microsoft Dynamics CRM.
        /// </summary>
        /// <returns>True for a valid string, otherwise False.</returns>
        private static Boolean IsValidConnectionString(String connectionString)
        {
            // At a minimum, a connection string must contain one of these arguments.
            if (connectionString.Contains("Url=") ||
                connectionString.Contains("Server=") ||
                connectionString.Contains("ServiceUri="))
                return true;

            return false;
        }

        #endregion Connection Code
    }
}
