﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecordRetentionPolicy
{
    public class Lead
    {
        public const string EntityName = "lead";
        public const string LeadPrimaryKey = "leadid";  //Unique identifier of the lead. 
        public const string ParentAccountID = "parentaccountid"; //Choose an account to connect this lead to, so that the relationship is visible in reports and analytics.
        public const string ParentContactID = "parentcontactid"; //Choose a contact to connect this lead to, so that the relationship is visible in reports and analytics.
        public const string FiveYearDelete = "seera_fiveyeardelete"; //Marker for if the record is over five years old
        public const string CreatedOn = "createdon";
        public const string Subject = "subject";
    }
}
